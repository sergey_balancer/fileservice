# FileService

##Example

    //init local adapter
    $local = new Adapter\Local();
    
    //init s3 adapter
    $s3 = new Adapter\S3([
        'bucket' => 'test',
        'profile' => 'default',
        'region' => 'us-west-2',
        'version' => '2019-01-25'
    ]);
    
    //init file storage
    $storage = new FileStorage($s3);

    //execute methods
    $storage->saveFile('/path/to/file.txt', 'Hello world');
    $storage->getFileContent('/path/to/file.txt');
    $storage->getFileInfo('/path/to/file.txt');
    $storage->deleteFile('/path/to/file.txt');
