<?php

use Adapter\AdapterInterface;

class FileStorage
{
    /**
     * @var AdapterInterface
     */
    private $adapter;

    /**
     * FileStorage constructor.
     * @param AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param string $path
     * @param string $content
     * @return bool
     */
    public function saveFile(string $path, string $content): bool
    {
        return $this->adapter->saveFile($path, $content);
    }

    /**
     * @param string $path
     * @return string
     */
    public function getFileContent(string $path): string
    {
        return $this->adapter->getFileContent($path);
    }

    /**
     * @param string $path
     * @return array
     */
    public function getFileInfo(string $path): array
    {
        return $this->adapter->getFileInfo($path);
    }

    /**
     * @param string $path
     * @return bool
     */
    public function deleteFile(string $path): bool
    {
        return $this->adapter->deleteFile($path);
    }
}
