<?php

namespace Adapter;

class Local implements AdapterInterface
{
    /**
     * @param string $path
     * @param string $content
     * @return bool
     */
    public function saveFile(string $path, string $content): bool
    {
        return file_put_contents($path, $content) !== false;
    }

    /**
     * @param string $path
     * @return string
     */
    public function getFileContent(string $path): string
    {
        return file_get_contents($path);
    }

    /**
     * @param string $path
     * @return array
     */
    public function getFileInfo(string $path): array
    {
        return pathinfo($path);
    }

    /**
     * @param string $path
     * @return bool
     */
    public function deleteFile(string $path): bool
    {
        return unlink($path);
    }
}
