<?php

namespace Adapter;

use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use InvalidArgumentException;

class S3 implements AdapterInterface
{
    /**
     * @var S3Client
     */
    private $s3Client;

    /**
     * @var string
     */
    private $bucket;

    /**
     * @var array
     */
    private $configRequires = ['bucket', 'profile', 'region', 'version'];

    /**
     * S3 constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->ensureConfig($config);

        $this->bucket = $config['bucket'];

        $this->s3Client = new S3Client([
            'profile' => $config['profile'],
            'region' => $config['region'],
            'version' => $config['version'],
        ]);
    }

    /**
     * @param string $path
     * @param string $content
     * @return bool
     */
    public function saveFile(string $path, string $content): bool
    {
        try {
            $this->s3Client->putObject([
                'Bucket' => $this->bucket,
                'Key' => $path,
                'Body' => $content
            ]);
            return true;
        } catch (AwsException $e) {
            return false;
        }
    }

    /**
     * @param string $path
     * @return null|string
     */
    public function getFileContent(string $path): ?string
    {
        try {
            $result = $this->s3Client->getObject([
                'Bucket' => $this->bucket,
                'Key' => $path,
            ]);
            return $result['Body'];
        } catch (AwsException $e) {
            return null;
        }
    }

    /**
     * @param string $path
     * @return array|null
     */
    public function getFileInfo(string $path): ?array
    {
        try {
            $result = $this->s3Client->headObject([
                'Bucket' => $this->bucket,
                'Key' => $path,
            ]);
            return $result->toArray();
        } catch (AwsException $e) {
            return null;
        }
    }

    public function deleteFile(string $path): bool
    {
        try {
            $this->s3Client->deleteObject([
                'Bucket' => $this->bucket,
                'Key' => $path,
            ]);
            return true;
        } catch (AwsException $e) {
            return false;
        }
    }

    /**
     * @param array $config
     * @throws InvalidArgumentException
     */
    private function ensureConfig(array $config)
    {
        foreach ($this->configRequires as $item) {
            if (empty($config[$item])) {
                throw new InvalidArgumentException('Invalid config: ' . $item);
            }
        }
    }
}
