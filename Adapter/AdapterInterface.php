<?php

namespace Adapter;

interface AdapterInterface
{
    /**
     * @param string $path
     * @param string $content
     * @return bool
     */
    public function saveFile(string $path, string $content): bool;

    /**
     * @param string $path
     * @return string
     */
    public function getFileContent(string $path): ?string;

    /**
     * @param string $path
     * @return array
     */
    public function getFileInfo(string $path): ?array;

    /**
     * @param string $path
     * @return bool
     */
    public function deleteFile(string $path): bool;
}
